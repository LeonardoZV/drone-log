package tech.mastertech.itau.logs.repositories;

import java.time.LocalDate;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.logs.models.Log;

public interface LogRepository extends CrudRepository<Log, LocalDate>{
	Iterable<Log> findAllByData(LocalDate data);
}
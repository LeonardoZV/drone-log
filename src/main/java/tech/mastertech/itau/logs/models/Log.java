package tech.mastertech.itau.logs.models;

import java.time.LocalDate;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Log {
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private LocalDate data;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID dataHora;
	@NotBlank
	private String descricao;

	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public UUID getDataHora() {
		return dataHora;
	}
	public void setDataHora(UUID dataHora) {
		this.dataHora = dataHora;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

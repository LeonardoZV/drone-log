package tech.mastertech.itau.logs.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.logs.models.Log;
import tech.mastertech.itau.logs.services.LogService;

@RestController
@RequestMapping("/log")
public class LogController {
	
	@Autowired
	LogService logService;
	
	@GetMapping
	public Iterable<Log> buscar(@RequestParam String data) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return logService.buscar(LocalDate.parse(data,formatter));
	}
}

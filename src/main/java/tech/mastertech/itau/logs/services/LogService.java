package tech.mastertech.itau.logs.services;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import tech.mastertech.itau.logs.models.Log;
import tech.mastertech.itau.logs.repositories.LogRepository;

@Service
public class LogService {

	@Autowired
	LogRepository logRepository;

	public void cadastrar(String descricao) {
		Log log = new Log();
		log.setData(LocalDate.now());
		log.setDataHora(UUIDs.timeBased());
		log.setDescricao(descricao);

		logRepository.save(log);
	}

	public Iterable<Log> buscar(LocalDate data) {
		return logRepository.findAllByData(data);
	}

	@KafkaListener(id = "log-consumer", topics = "drone-delivery")
	public void gerarLog(@Payload String descricao) {
		cadastrar(descricao);
	}

}
